<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LoginController extends Controller
{
    /**
     * @Route("/login", name="login")
     */
    public function index(AuthenticationUtils $utils)
    {
        $error = $utils->getLastAuthenticationError();
        $lastEmail = $utils->getLastUsername();

        return $this->render('log_in/index.html.twig', [
            'error' => $error,
            'lastEmail' => $lastEmail,
            'controller_name' => 'Login'
        ]);
    }
}
