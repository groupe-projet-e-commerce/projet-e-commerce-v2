<?php

namespace App\Repository;

use App\Entity\EditProduct;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EditProduct|null find($id, $lockMode = null, $lockVersion = null)
 * @method EditProduct|null findOneBy(array $criteria, array $orderBy = null)
 * @method EditProduct[]    findAll()
 * @method EditProduct[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EditProductRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EditProduct::class);
    }

//    /**
//     * @return EditProduct[] Returns an array of EditProduct objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EditProduct
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
